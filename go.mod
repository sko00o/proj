module gitlab.com/sko00o/proj

go 1.17

require gitlab.com/sko00o/demo v0.0.0-20220617073456-1743e7c33602

replace gitlab.com/sko00o/demo => gitlab.com/sko00o/demo-fork v0.0.0-20220617093215-a81b1acbb872
