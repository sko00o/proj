package main

import (
	"fmt"

	"gitlab.com/sko00o/proj/api"
)

func main() {
	fmt.Printf("api version %s\n", api.Version())
}
