package api

import "gitlab.com/sko00o/demo"

func Version() string {
	return demo.Version()
}
